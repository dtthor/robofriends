export const robots = [
  {
    id: 1,
    name: 'Alfredo Thorwaldson',
    username: 'Alfie',
    email: 'AlfT@me.com'
  },
  {
    id: 2,
    name: 'Katherine Thorwaldson',
    username: 'KittyThor',
    email: 'KT@me.com'
  },
  {
    id: 3,
    name: 'Dylan Thorwaldson',
    username: 'Dylan',
    email: 'DTT@me.com'
  },
  {
    id: 4,
    name: 'Moose Thor',
    username: 'Moose',
    email: 'MooseT@me.com'
  },
  {
    id: 5,
    name: 'Scout Guinn',
    username: 'Scooter',
    email: 'sg@me.com'
  },
  {
    id: 6,
    name: 'Christine Thor',
    username: 'ChrisT',
    email: 'ChrisT@me.com'
  },
  {
    id: 7,
    name: 'Josh Currie',
    username: 'Josh',
    email: 'JoshC@me.com'
  },
  {
    id: 8,
    name: 'Josh Marx',
    username: 'Jmarx',
    email: 'JoshM@me.com'
  },
  {
    id: 9,
    name: 'Kevin Adams',
    username: 'KA',
    email: 'KFA@me.com'
  },
  {
    id: 10,
    name: 'Ashley Barnosky',
    username: 'Ashb',
    email: 'Ashb@me.com'
  },
  {
    id: 11,
    name: 'David Barnosky II',
    username: 'DB2',
    email: 'DB2@me.com'
  },
  {
    id: 12,
    name: 'Abigail Barnosky',
    username: 'AGB143',
    email: 'AGB@me.com'
  },
  {
    id: 13,
    name: 'Hannah Barnosky',
    username: 'Banana',
    email: 'HB@me.com'
  },
  {
    id: 14,
    name: 'Watson Jones',
    username: 'WATJ',
    email: 'Watson.Jones@EY.com'
  },
  {
    id: 15,
    name: 'Baker Young',
    username: 'shakeandBAKE',
    email: 'Baker.Young@hotmailzzz.com'
  },
]
